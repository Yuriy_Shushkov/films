namespace Films.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFilmsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FilmsModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Poster = c.String(nullable: false),
                        Year = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Director = c.String(nullable: false),
                        Author = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FilmsModels");
        }
    }
}
