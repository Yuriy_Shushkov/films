namespace Films.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFilmsTable1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FilmsModels", "Author", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FilmsModels", "Author", c => c.String(nullable: false));
        }
    }
}
