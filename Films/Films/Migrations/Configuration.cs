namespace Films.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Films.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Films.Models.ApplicationDbContext";
        }

        protected override void Seed(Films.Models.ApplicationDbContext context)
        {
        }
    }
}
