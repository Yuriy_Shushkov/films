﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Films.Models
{		
	public class FilmsModels
	{
		[Key]
		[Required]
		public int Id { get; set; }

		[Required]
		[Display(Name = "Наименование")]
		public string Name { get; set; }

		[Required]
		[Display(Name = "Постер")]
		public string Poster { get; set; }

		[Required]
		[Display(Name = "Год")]
		public int Year { get; set; }

		[Required]
		[Display(Name = "Описание")]
		public string Description { get; set; }

		[Required]
		[Display(Name = "Режисcер")]
		public string Director { get; set; }

		[Display(Name = "Автор")]
		public string Author { get; set; }
	}
}