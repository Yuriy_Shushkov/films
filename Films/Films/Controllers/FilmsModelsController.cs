﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Films.Models;
using PagedList;

namespace Films.Controllers
{
    public class FilmsModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: FilmsModels
        public ActionResult Index(int? page)
        {
			int pageSize = 3;
			int pageNumber = (page ?? 1);
			ViewBag.User = User.Identity.Name; 
			return View(db.FilmsModels.OrderBy(x=>x.Id).ToPagedList(pageNumber, pageSize));			
        }

        // GET: FilmsModels/Details/5
        public ActionResult Details(int? id)
        {
			ViewBag.User = User.Identity.Name;
			if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmsModels filmsModels = db.FilmsModels.Find(id);
            if (filmsModels == null)
            {
                return HttpNotFound();
            }
            return View(filmsModels);
        }

        // GET: FilmsModels/Create
        public ActionResult Create()
        {
			ViewBag.User = User.Identity.Name;
			return View();
        }

        // POST: FilmsModels/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Poster,Year,Description,Director")] FilmsModels filmsModels)
        {
            if (ModelState.IsValid)
            {
				filmsModels.Author = User.Identity.Name;
				db.FilmsModels.Add(filmsModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(filmsModels);
        }

        // GET: FilmsModels/Edit/5
        public ActionResult Edit(int? id)
        {				
			if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmsModels filmsModels = db.FilmsModels.Find(id);
            if (filmsModels == null)
            {
                return HttpNotFound();
            }
            return View(filmsModels);
        }

        // POST: FilmsModels/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Poster,Year,Description,Director,Author")] FilmsModels filmsModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filmsModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(filmsModels);
        }

        // GET: FilmsModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FilmsModels filmsModels = db.FilmsModels.Find(id);
            if (filmsModels == null)
            {
                return HttpNotFound();
            }
            return View(filmsModels);
        }

        // POST: FilmsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FilmsModels filmsModels = db.FilmsModels.Find(id);
            db.FilmsModels.Remove(filmsModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
